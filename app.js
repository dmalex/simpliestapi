const express = require("express")
const router = express.Router()
const app = express()
app.use("/", router)

router.get("/", (req, res) => {
    res.send({"kind": "success"})
})

const port = 8080
app.listen(port, () => {
    console.log(`Simpliest API started on port ${port}`)
})